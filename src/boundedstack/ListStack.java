package boundedstack;

import java.util.ArrayList;
import java.util.List;

public class ListStack<E> implements Stack<E> {
    private List<E> contents;
    private final int capacity;

    public ListStack(int capacity) {
        if (capacity <= 0) throw new IllegalArgumentException("Capacity must be non-negative and non-zero!");
        this.contents = new ArrayList<>();
        this.capacity = capacity;
    }

    /**
     * Adds the specified element to the top of this stack
     *
     * @param element the element to be added to this stack
     * @throws IllegalStateException if this stack is full
     */
    @Override
    public void push(E element) {
        if (element == null) throw new IllegalArgumentException("Element cannot be null!");
        if (contents.size() == capacity) throw new IllegalStateException("Stack is full!");
        contents.add(element);
    }

    /**
     * Removes and returns an element from the top of this stack
     *
     * @return the element removed from this stack
     * @throws IllegalStateException if this stack is empty
     */
    @Override
    public E pop() {
        if (contents.isEmpty()) throw new IllegalStateException("Stack is empty!");
        return contents.remove(contents.size() - 1);
    }

    /**
     * Returns the depth of this stack
     *
     * @return the number of elements currently in this stack
     */
    @Override
    public int depth() {
        return contents.size();
    }

    /**
     * Returns the capacity of this stack
     *
     * @return the maximum number of elements this stack can hold
     */
    @Override
    public int capacity() {
        return capacity;
    }
}
