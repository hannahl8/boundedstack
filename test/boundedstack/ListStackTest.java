package boundedstack;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class ListStackTest {

    private Stack<String> empty3; // empty stack with capacity of 3
    private Stack<String> abc6; // stack with elements A, B, and C with capacity of 6

    @Before
    public void setUp() {
        empty3 = new ListStack<>(3);
        abc6 = new ListStack<>(6);
        abc6.push("A");
        abc6.push("B");
        abc6.push("C");
    }

    @Test
    public void testInitialStacks() {
        assertEquals(0, empty3.depth());
        assertEquals(3, empty3.capacity());

        assertEquals(3, abc6.depth());
        assertEquals(6, abc6.capacity());
    }

    // Note: The push method is tested by the setup test.

    @Test
    public void pop() {
        assertEquals("C", abc6.pop());
        assertEquals(2, abc6.depth());
    }
    // Note: depth and capacity are simple getters that are used to test other methods.
}